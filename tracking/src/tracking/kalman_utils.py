#! /usr/bin/env python

import numpy as np

uncertainty_gain = 1000

eye = np.array([[1., 0., 0.],
                     [0., 1., 0.],
                     [0., 0., 1.]])

kalman_sensor_dict = {
    "nose": eye,
    "eye": eye,
    "ear": eye,
    "shoulder": eye,
    "elbow": eye,
    "wrist": eye,
    "hip": eye,
    "knee": eye,
    "ankle": eye,
    "centroid": eye,
    "laser": eye,
    "velocity": eye*0.0001,
    "missing": eye*0.1
}

for index,value in kalman_sensor_dict.items():
    if index not in ["velocity", "missing", "laser", "centroid"]:
        value[0][0] = 0.0018
        value[1][1] = 0.0072
        value[2][2] = 0.0722

kalman_sensor_dict["centroid"] = np.array(
                [[0.1, 0., 0.],
                 [0., 0.001, 0.],
                 [0., 0., 0.1]])

kalman_sensor_dict["laser"] = np.array(
                [[0.001, 0., 0.],
                 [0., 10., 0.],
                 [0., 0., 0.001]])

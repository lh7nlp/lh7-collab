#! /usr/bin/env python
import rospy
import moveit_commander
import numpy as np
from rospy_message_converter import message_converter
from geometry_msgs.msg import PoseStamped, Pose
from moveit_msgs.msg import CollisionObject
from human_pose_ROS.msg import Skeleton, PoseEstimation, Limbs
from vision_utils.logger import get_logger

logger = get_logger()

collision_joints = ["nose","eye","wrist","shoulder","elbow", "ear"]

connected_points = {
'eyeshoulder_left': (1,5),
'eyeshoulder_right': (2,6),
'eyear_left': (2,4),
'eyear_right': (3,5),
'noseye_left': (0,1),
'noseye_right': (0,2),
'torso_left': (5,11),
'torso_right': (6,12),
'torso_top': (5,6),
'torso_bottom': (11,12),
'upperarm_left': (5,7),
'upperarm_right': (6,8),
'forearm_left': (7,9),
'forearm_right': (8,10),
'thigh_left':(11,13),
'thigh_right':(12,14),
'calf_left':(13,15),
'calf_right':(14,16)
}

openpifpaf_pairs = {0: 'nose', 1: 'left_eye', 2: 'right_eye', 3: 'left_ear', 4:
'right_ear', 5: 'left_shoulder', 6: 'right_shoulder', 7: 'left_elbow', 8:
'right_elbow', 9: 'left_wrist', 10: 'right_wrist', 11: 'left_hip', 12:
'right_hip', 13: 'left_knee', 14: 'right_knee', 15: 'left_ankle', 16:
'right_ankle'}

limb_per_kp = {
    'nose': 'noseye',
    'eye': 'noseye',
    'shoulder': 'upperarm',
    'wrist': 'forearm',
    'elbow': 'upperarm',
    'ear': 'eyear'
}

limb_lengths = dict()

def human_cb(msg):

    for skel in msg.skeletons:
            pose = skel.centroid
            # print("Pose: ", pose)
            msg_dict = message_converter.convert_ros_message_to_dictionary(skel)
            lst_x = [v[0] for k,v in msg_dict.items() if isinstance(v,list) and len(v)]
            lst_y = [v[1] for k,v in msg_dict.items() if isinstance(v,list) and len(v)]
            lst_z = [v[2] for k,v in msg_dict.items() if isinstance(v,list) and len(v)]

            scale_x = np.max(lst_x) - np.min(lst_x)
            scale_y = np.max(lst_y) - np.min(lst_y)
            scale_z = np.max(lst_z) - np.min(lst_z)

            box_pose = PoseStamped()
            box_pose.header.frame_id = "world"
            box_pose.pose.orientation.w = 1.0

            box_pose.pose.position.x = np.min(lst_x) + float(scale_x/2)
            box_pose.pose.position.y = np.min(lst_y) + float(scale_y/2)
            box_pose.pose.position.z = np.min(lst_z) + float(scale_z/2)

            # scene.add_box(str(skel.id), box_pose, size=(scale_x, scale_y, scale_z))

            # Add spheres around all joints
            for k,v in msg_dict.items():
                joint_name = k.split("_")[-1]
                side = k.split("_")[0] if len(k.split("_")) > 1 else "left"
                if isinstance(v,list) and len(v) and joint_name in collision_joints:
                    sphere_pose = PoseStamped()
                    sphere_pose.header.frame_id = "world"
                    sphere_pose.pose.position.x = v[0]
                    sphere_pose.pose.position.y = v[1]
                    sphere_pose.pose.position.z = v[2]
                    sphere_pose.pose.orientation.w = 1.0

                    # if len(v)>3:
                    #     uncertainty = v[3:6]
                    #     radius = min(0.5,max(uncertainty)/2)
                    # else:
                    #     radius = 0.5

                    max_lengths = dict()
                    if joint_name in limb_per_kp:
                        limb = limb_per_kp[joint_name] + '_' + side

                        if limb_lengths[limb] > 0:
                            radius = limb_lengths[limb]/2
                            # scene.add_sphere(str(skel.id)+k, sphere_pose, radius=radius)
                        else:
                            logger.warning("{}: {}".format(limb, limb_lengths[limb]))




def limbs_cb(msg):
    global limb_lengths
    limb_lengths = message_converter.convert_ros_message_to_dictionary(msg)
    # print(limb_lengths)

rospy.init_node("add_collision_node")

collision_object_pub = rospy.Publisher('/collision_object', CollisionObject, queue_size=10)
limb_sub = rospy.Subscriber('limbs_after', Limbs, limbs_cb)

scene = moveit_commander.PlanningSceneInterface()
rospy.sleep(0.3)

# Clearing all objects in the planning scene
objects = scene.get_objects()
for obj in objects:
    scene.remove_world_object(obj)


wall_pose = PoseStamped()
wall_pose.header.frame_id = "world"
wall_pose.pose.position.x = 1.0
wall_pose.pose.position.y = 0.5
wall_pose.pose.position.z = 1
wall_pose.pose.orientation.w = 1.0
scene.add_box("wall_right", wall_pose, size=(0.01,2,2))

wall_pose = PoseStamped()
wall_pose.header.frame_id = "world"
wall_pose.pose.position.x = 0
wall_pose.pose.position.y = 0.5
wall_pose.pose.position.z = 2
wall_pose.pose.orientation.w = 1.0
scene.add_box("wall_top", wall_pose, size=(2,2,0.01))

wall_pose = PoseStamped()
wall_pose.header.frame_id = "world"
wall_pose.pose.position.x = -1
wall_pose.pose.position.y = 0.5
wall_pose.pose.position.z = 1
wall_pose.pose.orientation.w = 1.0
scene.add_box("wall_left", wall_pose, size=(0.01,2,2))

wall_pose = PoseStamped()
wall_pose.header.frame_id = "world"
wall_pose.pose.position.x = 0
wall_pose.pose.position.y = -0.5
wall_pose.pose.position.z = 1
wall_pose.pose.orientation.w = 1.0
scene.add_box("wall_back", wall_pose, size=(2,0.01,2))


# Initialize collision obejct
# object_msg = CollisionObject()
# object_msg.operation = object_msg.MOVE
# object_msg.id = "box"
# object_msg.header.stamp = rospy.Time.now()
# object_msg.header.frame_id = "world"

# new_pose = Pose()
# new_pose.position.x = pose[0]
# new_pose.position.y = pose[1]
# new_pose.position.z = pose[2]
# new_pose.orientation.w = 1.0

# # Using new pose to move collision object
# object_msg.mesh_poses = [new_pose]
# collision_object_pub.publish(object_msg)

rospy.Subscriber("/openpifpaf_pose_transformed_kalman_world", PoseEstimation, human_cb)

rospy.spin()

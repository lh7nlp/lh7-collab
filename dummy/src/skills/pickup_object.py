#! /usr/bin/env python

import rospy
from robot_moveit_commander import robot_class as robot
from geometry_msgs.msg import Pose

def work_CB(msg):
    print(msg)

rospy.init_node('dummy_move')

move_sub = rospy.Subscriber("/working_commander", String, work_CB)

robot_client = robot.Robot()
robot_client.rs_gripper.genCommand("activate")
robot_client.rs_gripper.genCommand("open")

robot_client.setNamedTarget("ready", "right", move=True)

group = robot_client.right_arm
# current_pose = group.get_current_pose().pose
# print(current_pose)



while not rospy.is_shutdown():

    pose = Pose()
    pose.position.x = 0.24052026287
    pose.position.y = 0.263829171294
    pose.position.z = 1.16759758744

    pose.orientation.x = -0.502154342153
    pose.orientation.y = 0.509991971661
    pose.orientation.z = 0.47373413243
    pose.orientation.w = 0.513152196984

    #Move above object
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)

    #Move down to object
    pose.position.z -= 0.1
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)

    #Close gripper
    robot_client.rs_gripper.genCommand("close")

    #Move up with the object
    pose.position.z += 0.1
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)

    #Move 20 cm to the left with the object
    pose.position.x -= 0.2
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)

    #Move down to object
    pose.position.z -= 0.1
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)

    robot_client.rs_gripper.genCommand("open")
    robot_client.rs_gripper.genCommand("close")

    #Move up with the object
    pose.position.z += 0.1
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)

    #Move 20 cm to the right with the object
    pose.position.x += 0.2
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)

    #Move down to object
    pose.position.z -= 0.1
    plan, error = robot_client.setTarget(pose, "right", move=True, return_plan=True)
    robot_client.rs_gripper.genCommand("open")

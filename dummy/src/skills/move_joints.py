#! /usr/bin/env python

from robot_moveit_commander import robot_class as robot
import rospy
import yaml
from std_msgs.msg import Bool, String
from geometry_msgs.msg import Pose
from vision_utils.logger import get_logger, get_printer

SIM = False
def move_joints_set_sim(msg):
    global SIM
    SIM = msg.data

import pathlib
script_path = pathlib.Path(__file__).parent.absolute()


logger = get_logger()
pprinter = get_printer()

rospy.init_node("move_commander")

robot_client = robot.Robot()
robot_client.rs_gripper.genCommand("activate")


print("Dummy move ready")

group_right = robot_client.right_arm
group_gripper = robot_client.gripper_group

# group_right.set_planner_id("BiTRRT")
# group_right.set_planning_time(1)
# group_gripper.set_planner_id("BiTRRT")
# group_gripper.set_planning_time(1)

move_pos = True

move=True

work_states = ["picking", "work_left", "placing",
              "picking", "work_right", "placing"]
state_counter = 0

pose = Pose()
pose.position.x = 0.24052026287
pose.position.y = 0.263829171294
pose.position.z = 1.16759758744

pose.orientation.x = -0.502154342153
pose.orientation.y = 0.509991971661
pose.orientation.z = 0.47373413243
pose.orientation.w = 0.513152196984

wave_pose = [-0.5232513586627405, -1.2941420714007776, -2.0736411253558558, -1.4272764364825647, -1.884775463734762, 0.8888819813728333]
handover_pose = [-0.33431798616518194, -2.8362882773028772, -1.129157845173971, -0.0029318968402307632, 0.7364381551742554, -0.2716906706439417]

idx_to_joint_name = dict()
joint_name_to_idx = dict()

idx_to_joint_name['gripper'] = {i: name for i, name in enumerate(group_gripper.get_joints())}
idx_to_joint_name['right'] = {i: name for i, name in enumerate(group_right.get_joints())}

print(idx_to_joint_name['gripper'])
print(idx_to_joint_name['right'])

robot_client.setNamedTarget("ready", "right", move=True)

for group in idx_to_joint_name.keys():
    joint_name_to_idx[group] = {v: k for k,v in idx_to_joint_name[group].items()}

def check_plan(plan, group_name, values=dict(), velocity_threshold = 0.0001, accel_threshold = 0.0001):
    is_valid_plan = True
    for point in plan.joint_trajectory.points:
        for joint in joint_name_to_idx[group_name].keys():
            try:
                joint_velocity = abs(point.velocities[joint_name_to_idx[group_name][joint]])
                joint_accel = abs(point.accelerations[joint_name_to_idx[group_name][joint]])
                if joint not in values:
                        if joint_velocity > velocity_threshold or joint_accel > accel_threshold:
                            is_valid_plan = False
                            logger.warning("Plan check: {} shouldn't move but the plan says it should".format(joint))
                #
                # else:
                #     logger.debug("{} | velocity {}, accel {}".format(joint, joint_velocity, joint_accel))
            except IndexError:
                pass

    # logger.debug("Is valid plan? {}".format(is_valid_plan))
    return is_valid_plan

def go_to_wave():
    move_pub.publish("waving to_waving_pose")
    state = robot_client.robot.get_current_state()
    group = group_right
    group.set_start_state(state)
    new_pose = wave_pose
    group.set_joint_value_target(new_pose)

    plan = group.plan()
    if move: group.execute(plan, wait=True)

def go_to_handover():
    move_pub.publish("handover to_handover_pose")
    state = robot_client.robot.get_current_state()
    group = group_right
    group.set_start_state(state)
    new_pose = handover_pose
    group.set_joint_value_target(new_pose)

    plan = group.plan()
    if move: group.execute(plan, wait=True)
    move_pub.publish("handover open_gripper")
    open_gripper()
    move_pub.publish("handover done")

def set_joints(group, group_name, values=dict(), add_mode=False):
    current_pose = group.get_current_joint_values()
    # pprinter.pprint({i:idx_to_joint_name[group_name][i] for i, _ in enumerate(current_pose)})
    state = robot_client.robot.get_current_state()
    group.set_start_state(state)
    new_pose = current_pose
    for joint, value in values.items():
        try:
            if add_mode:
                new_pose[joint_name_to_idx[group_name][joint]] += value
            else:
                new_pose[joint_name_to_idx[group_name][joint]] = value
        except IndexError:
            logger.warning("Can't set {}, skipping".format(joint))
    group.set_joint_value_target(new_pose)
    plan = group.plan()
    # print(plan.joint_trajectory.points)

    if check_plan(plan, group_name, values):
        logger.info("Plan passed the check, executing")
        if move: group.execute(plan, wait=True)
        return True
    return False

def save_plan(traj_name, plan):
    file_path = script_path.parent / 'saved_trajectories' / traj_name
    with open(str(file_path), 'w') as file_save:
        yaml.dump(plan, file_save, default_flow_style=True)


def open_gripper():
    global status

    if SIM:
        # print("Running in simulation")
        joint_values = {
            'finger_1_joint_1': 0,
            'finger_1_joint_2': 0,
            'finger_1_joint_3': 0,
            'finger_2_joint_1': 0,
            'finger_2_joint_2': 0,
            'finger_2_joint_3': 0,
            'finger_middle_joint_1': 0,
            'finger_middle_joint_2': 0,
            'finger_middle_joint_3': 0,
            'palm_finger_1_joint': 0.25,
            'palm_finger_2_joint': 0,
            'palm_finger_middle_joint': 0
            }

        set_joints(group_gripper, group_name="gripper", values=joint_values)
    else:
        robot_client.rs_gripper.genCommand("open")
    status["gripper"] = "open"

def close_gripper():
    global status

    if SIM:
        # print("Running in simulation")
        joint_values = {
            'finger_1_joint_1': 1,
            'finger_1_joint_2': 1,
            'finger_1_joint_3': 1,
            'finger_2_joint_1': 1,
            'finger_2_joint_2': 1,
            'finger_2_joint_3': 1,
            'finger_middle_joint_1': 1,
            'finger_middle_joint_2': 1,
            'finger_middle_joint_3': 1,
            'palm_finger_1_joint': 0,
            'palm_finger_2_joint': 0,
            'palm_finger_middle_joint': 0
            }

        set_joints(group_gripper, group_name="gripper", values=joint_values)
    else:
        robot_client.rs_gripper.genCommand("close")
    status["gripper"] = "closed"

def wave_right():
    global move_pos
    for waves in range(1):
        joint_values = {
            'ur5_right_wrist_2_joint': 1 if move_pos else -1
        }
        if move_pos: move_pub.publish("waving wave_right")
        else: move_pub.publish("waving wave_left")
        if(set_joints(group_right, group_name="right", values=joint_values, add_mode=True)):
            move_pos = not move_pos
    move_pub.publish("waving done")

prev_work_step = "none"
next_work_step = {
    "none": "move_to_object",
    "move_to_object": "down_to_object",
    "down_to_object": "close_gripper",
    "close_gripper": "up_with_object",
    "up_with_object": "move_to_object"
}

def grasp_object(grasp="pick", arm="right"):
    global pose, prev_work_step
    # #Move above target
    # pose.position.z += 0.1
    step = next_work_step[prev_work_step]

    move_pub.publish("working {}".format(step))
    if step == "move_to_object":
        if move: plan, error = robot_client.setTarget(pose, arm, move=True, return_plan=True)
    elif step == "down_to_object":
        pose.position.z -= 0.1
        if move: plan, error = robot_client.setTarget(pose, arm, move=True, return_plan=True)
    elif step == "close_gripper":
        close_gripper()
    elif step == "open_gripper":
        open_gripper()

    elif step in ["up_with_object", "up_without_object"]:
        pose.position.z += 0.1
        if move: plan, error = robot_client.setTarget(pose, arm, move=True, return_plan=True)

    # if move: robot_client.linear_move("right", pose)
    #Move down to target
    # move_pub.publish("working down_to_object")

    # if move: robot_client.linear_move("right", pose)
    #close gripper and move up
    # if grasp == "pick":
    #     move_pub.publish("working close_gripper")
    #     close_gripper()
    #     move_pub.publish("working up_with_object")
    # elif grasp == "place":
    #     move_pub.publish("working open_gripper")
    #     open_gripper()
    #     move_pub.publish("working up_without_object")
    # else:
    #     logger.warning("Unkown grasp mode")


    # if move: robot_client.linear_move("right", pose)



    prev_work_step = step
    move_pub.publish("working done")


def next_work():
    global state_counter
    state_counter += 1
    if state_counter > 5:
        state_counter = 0
    status["next_work"] = work_states[state_counter]


def picking():
    status["work"] = "picking"
    logger.warning("PICKING!!!!!!!")
    grasp_object(grasp="pick")
    next_work()


def placing():
    status["work"] = "placing"
    grasp_object(grasp="place")
    next_work()


def work_left():
    global pose
    status["work"] = "work_left"
    pose.position.x -= 0.2
    move_pub.publish("working left_with_object")
    next_work()

def work_right():
    global pose
    status["work"] = "work_right"
    pose.position.x += 0.2
    move_pub.publish("working right_with_object")
    next_work()

move_commands = {
    'open_gripper': open_gripper,
    'close_gripper': close_gripper,
    'wave': wave_right,
    'work': grasp_object,
    "handover": go_to_handover
}

status = {
    'gripper': None,
    'wave': "none",
    "work": "none",
    "next_work": "picking",
    "handover": "none"
}

working_commands = {
    "picking": picking,
    "placing": placing,
    "work_left": grasp_object,
    "work_right": grasp_object
}

new_command = False
latest_command = None

def move_CB(msg):
    global status, new_command, latest_command
    if msg.data in move_commands:

        latest_command = msg.data
        new_command = True


move_pub = rospy.Publisher("/move_status", String, queue_size=1)
move_sub = rospy.Subscriber("/move_commander", String, move_CB)
sim_sub = rospy.Subscriber("/use_sim", Bool, move_joints_set_sim)

print("MOVE READY")

def toggle_gripper():

    if status["gripper"] == "closed":
        open_gripper()
    else:
        close_gripper()


while not rospy.is_shutdown():
    if new_command:

        if latest_command == 'close_gripper' and status['gripper'] in ['open', None]:
            logger.debug('Closing gripper')
            move_commands[latest_command]()
            status['gripper'] = 'closed'
            status['wave'] = 'none'
        elif latest_command == 'open_gripper' and status['gripper'] in ['closed', None]:
            logger.debug('Opening gripper')
            move_commands[latest_command]()
            status['gripper'] = 'open'
            status['wave'] = 'none'
        elif latest_command == "handover" and status['handover'] != 'handover':
            logger.debug('Going to handover pose')
            if status['handover'] == 'none':
                move_commands[latest_command]()
                # status['handover'] = "handover_pose"
            status["handover"] = "handover"
            toggle_gripper()
            status['wave'] = 'none'
        elif latest_command == 'wave':

            if status['wave'] == 'none':
                logger.debug("Going to wave")
                go_to_wave()
            logger.debug('Waving')
            status['wave'] = 'waving'
            move_commands[latest_command]()
            status['wave'] = 'done'
            status["handover"] = "none"
        # elif latest_command == 'work' and status['work'] != 'working':
        #     logger.debug('working')
        #     status['work'] = 'working'
        #     move_commands[latest_command]()
        #     status['work'] = 'done'
        elif latest_command == 'work':
            move_commands[latest_command]()
            status['wave'] = 'none'
            status["handover"] = "none"
        new_command = False

        #rospy.sleep(2)

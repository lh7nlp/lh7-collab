#! /usr/bin/env python3

import rospy
from std_msgs.msg import ColorRGBA, String
from vision_utils.logger import get_logger

from random import randint

logger=get_logger()

colors = {
    "red": (255, 0, 0),
    "green": (0, 255, 0),
    "blue": (0, 0, 255),
    "yellow": (255, 255, 0),
    "black": (0, 0, 0),
    "orange": (255,15,0)
}

flash = False

already_flashing = False

current_color = None

def led_CB(msg):
    # logger.debug("LED: got msg {}".format(msg.data))
    color = msg.data.split(" ")[-1]
    if color == "random":
        flash_leds((randint(0,255),randint(0,255),randint(0,255)))
    elif color in colors.keys():
        print(f"current color: {current_color} - color: {color}")
        if "blink" in msg.data:
            if not already_flashing: flash_leds(color)
        else:
            if color != current_color: set_color(color)
    else:
        logger.error("Please give me a color I actually know thanks")

import time
last_flash = time.time()

def flash_leds(color):
    global flash, already_flashing, last_flash, current_color

    already_flashing = True
    if time.time() - last_flash  > 0.3:
        if flash:
            color_msg = get_color_msg(color)
            current_color = color
        else:
            color_msg = get_color_msg("black")
        flash = not flash
        last_flash = time.time()
        pub.publish(color_msg)
    # logger.debug("I AM FLASHY AS FUCK")

    already_flashing = False

def get_color_msg(color):
    color_msg = ColorRGBA()
    if isinstance(color,str):
        color_msg.r, color_msg.g, color_msg.b = colors[color]
        logger.debug(f"I AM {color.upper()}")
    else:
        color_msg.r, color_msg.g, color_msg.b = color
    return color_msg

def set_color(color):
    global current_color
    color_msg = get_color_msg(color)
    current_color = color
    pub.publish(color_msg)


rospy.init_node("LED_commander")

pub = rospy.Publisher("/led_strip", ColorRGBA, queue_size=1)
sub = rospy.Subscriber("/led_commander", String, led_CB)

print("LEDS READY")
rospy.spin()

#! /usr/bin/env python

import rospy
from robot_moveit_commander import robot_class as robot

rospy.init_node("CLOSE_HAND_NODE")

robot_client = robot.Robot()
rospy.sleep(1)
robot_client.rs_gripper.genCommand("close")
print("successfully CLOSED THE HAND")

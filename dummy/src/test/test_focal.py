#! /usr/bin/env python

from __future__ import print_function

import rospy
import tf
import cv2
import numpy as np
from math import pi
from cv_bridge import CvBridge

from visualization_msgs.msg import Marker, MarkerArray
from tf.transformations import quaternion_from_euler

from robot_moveit_commander import robot_class as robot
from perception import CameraIntrinsics
from autolab_core import Point

from std_msgs.msg import String, Bool
from webots_ros.srv import set_int
from sensor_msgs.msg import Image
from geometry_msgs.msg import Pose
from actionlib_msgs.msg import GoalStatusArray

def left_CB(msg):
    global robot_client
    for i in range(0, len(msg.status_list)):
        if msg.status_list[i].status is 2 or 4:
            robot_client.left_result = True
            break

def right_CB(msg):
    global robot_client
    for i in range(0, len(msg.status_list)):
        if msg.status_list[i].status is 2:
            robot_client.right_result = True
            break

def make_marker(id, x, y, z, scale=0.1):
    # Create marker at object location
    marker = Marker()
    marker.header.stamp = rospy.Time.now()
    marker.header.frame_id = 'world'
    marker.type = 2
    marker.id = id

    marker.pose.position.x = x
    marker.pose.position.y = y
    marker.pose.position.z = z

    marker.color.a = 1.0
    marker.color.r = 0.0
    marker.color.g = 1.0
    marker.color.b = 0.0

    marker.scale.x = scale
    marker.scale.y = scale
    marker.scale.z = scale

    marker.pose.orientation.x = 0
    marker.pose.orientation.y = 0
    marker.pose.orientation.z = 0
    marker.pose.orientation.w = 1

    return marker

def create_pose(position, rotation):
    pose = Pose()
    pose.position.x = position[0]
    pose.position.y = position[1]
    pose.position.z = position[2]

    pose.orientation.x = rotation[0]
    pose.orientation.y = rotation[1]
    pose.orientation.z = rotation[2]
    pose.orientation.w = rotation[3]

    return pose

def load_img(camera):

    # model_name = ""
    # if camera is "base":
        # Get model name of the camera.
        # This name changes every time the simulation is started
    frame_id = "base_camera_color_optical_frame"
        # model_mgs = rospy.wait_for_message("/model_name", String)
    #     model_name = model_mgs.data

    #     # Enable (turn on) camera and depth, with ROSservice
    #     camera_service = rospy.ServiceProxy(model_name + "/camera/enable", set_int)
    #     range_finder_service = rospy.ServiceProxy(model_name + "/range_finder/enable", set_int)
    #     camera_service.call(1)
    #     range_finder_service.call(1)
    # elif camera is "wrist":
    #     bool_yes = Bool()
    #     bool_yes.data = True
    #     global wrist_pub
    #     # Request camera image with boolean
    #     wrist_pub.publish(bool_yes)
    #     frame_id = "wrist_camera_color_optical_frame"
    #     model_name = "wrist"
    # else:
    #     print("Unkown camera specified")
    #     exit()

    # Get the raw depth and color images as ROS `Image` objects.
    print("WAITING FOR IMAGES")
    raw_color = rospy.wait_for_message('/base_camera/color/image_raw', Image)
    raw_depth = rospy.wait_for_message('/base_camera/aligned_depth_to_color/image_raw', Image)
    print("IMAGES LOADED")


    return raw_color, raw_depth, frame_id

def pixel_to_world(x, y, focal_length):
    """Transforms a 2D image point, to a 3D point in the world frame.

    Keyword arguments:
    x            -- x pixel value
    y            -- y pixel value
    focal_length -- focal length in pixels
    """

    HEIGHT = 720
    WIDTH = 1280

    # Wrap the camera info in a BerkeleyAutomation/perception
    # `CameraIntrinsics` object.
    camera_intr = CameraIntrinsics(
        frame_id, focal_length, focal_length,
        WIDTH/2, HEIGHT/2, 0.0, HEIGHT, WIDTH)

    # Wrap the pixel values in a 'Point' object
    data_point = np.array([x, y])
    point2D = Point(data_point, camera_intr.frame)
    # Convert 2D point to 3D point
    point3D = camera_intr.deproject_pixel(depth_im[y][x], point2D)

    # Transform point from camera frame to world frame
    tf_listener = tf.TransformListener()
    tf_listener.waitForTransform('/world', frame_id, rospy.Time(), rospy.Duration(4))
    (trans, rot) = tf_listener.lookupTransform('/world', frame_id, rospy.Time())

    world_to_cam = tf.transformations.compose_matrix(translate=trans, angles=tf.transformations.euler_from_quaternion(rot))
    obj_vector = np.concatenate((point3D.data, np.ones(1))).reshape((4, 1))
    obj_base = np.dot(world_to_cam, obj_vector)

    return np.array([obj_base[0][0], obj_base[1][0], obj_base[2][0]])


# Initialize ROS node, subscribers and publisher
rospy.init_node("focal_calibration_node")
rospy.Subscriber("/left_controller/follow_joint_trajectory/status", GoalStatusArray, left_CB)
rospy.Subscriber("/right_controller/follow_joint_trajectory/status", GoalStatusArray, right_CB)
markerArray_pub = rospy.Publisher('markerArray', MarkerArray, queue_size=1)
wrist_pub = rospy.Publisher("publish_images", Bool, queue_size=1)

markerarray = MarkerArray()

robot_client = robot.Robot()

robot_client.setNamedTarget("home", "left")
robot_client.setNamedTarget("ready", "right")

CAMERA = "base"
vis_input_img = True
pixel_sets = np.array([[249, 73], [54, 476], [636, 77], [738, 77], [1128, 77], [696, 547]])


# Prepare wrist camera for grasp
if CAMERA is "wrist":
    q_rot = quaternion_from_euler(0, pi, 0)
    robot_pose = create_pose((-0.3, 0.0, 1.7), q_rot)
    robot_client.setTarget(robot_pose, "right")

# Load images and convert to cv
cv_bridge = CvBridge()
raw_color, raw_depth, frame_id = load_img(CAMERA)
raw_color = cv_bridge.imgmsg_to_cv2(raw_color, "bgr8")
depth_im = cv_bridge.imgmsg_to_cv2(raw_depth, "32FC1")
depth_im = np.float32(cv2.normalize(depth_im, None, 0, 1, norm_type=cv2.NORM_MINMAX))
# Draw pixel points
for i in range(len(pixel_sets)):
    center_coordinates = (pixel_sets[i][0], pixel_sets[i][1])
    raw_color = cv2.circle(raw_color, center_coordinates, 10, (0, 0, 255), 2)

while vis_input_img:
    cv2.imshow("Depth Image", depth_im)
    cv2.imshow("Color Image", raw_color)

    k = cv2.waitKey(100) # change the value from the original 0 (wait forever)
    if k == 27:
        cv2.destroyAllWindows()
        break

for j in range(600,1000):
    print(j)
    for i in range(len(pixel_sets)):
        temp_point3D = pixel_to_world(pixel_sets[i][0], pixel_sets[i][1], j)
        temp_marker = make_marker(i+1, temp_point3D[0], temp_point3D[1], temp_point3D[2], 0.04)
        markerarray.markers.append(temp_marker)
        
        
    markerArray_pub.publish(markerarray)
    markerarray = MarkerArray()
    rospy.sleep(0.1)
rate = rospy.Rate(1)

while not rospy.is_shutdown():
    markerArray_pub.publish(markerarray)

    rate.sleep()

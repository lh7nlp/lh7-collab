#! /usr/bin/env python

from robot_moveit_commander import robot_class as robot
import rospy
import yaml
from std_msgs.msg import Bool, String
from geometry_msgs.msg import Pose
from vision_utils.logger import get_logger, get_printer

import random
import time
import csv
from pathlib import Path
from safety.msg import RobotArm, Distance

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--pipeline', default="ompl")
args, unknown = parser.parse_known_args()

project_path = Path(__file__).parent.absolute()

logger = get_logger()
pprinter = get_printer()

rospy.init_node("plot_plans")

robot_client = robot.Robot()

group_right = robot_client.right_arm
group_gripper = robot_client.gripper_group

idx_to_joint_name = dict()
joint_name_to_idx = dict()

idx_to_joint_name['gripper'] = {i: name for i, name in enumerate(group_gripper.get_joints())}
idx_to_joint_name['right'] = {i: name for i, name in enumerate(group_right.get_joints())}

print(idx_to_joint_name['gripper'])
print(idx_to_joint_name['right'])

for group in idx_to_joint_name.keys():
    joint_name_to_idx[group] = {v: k for k,v in idx_to_joint_name[group].items()}

def check_plan(plan, group_name, values=dict(), velocity_threshold = 0.0001, accel_threshold = 0.0001):
    is_valid_plan = True
    for point in plan.joint_trajectory.points:
        for joint in joint_name_to_idx[group_name].keys():
            try:
                joint_velocity = abs(point.velocities[joint_name_to_idx[group_name][joint]])
                joint_accel = abs(point.accelerations[joint_name_to_idx[group_name][joint]])
                if joint not in values:
                        if joint_velocity > velocity_threshold or joint_accel > accel_threshold:
                            is_valid_plan = False
                            logger.warning("Plan check: {} shouldn't move but the plan says it should".format(joint))
                #
                # else:
                #     logger.debug("{} | velocity {}, accel {}".format(joint, joint_velocity, joint_accel))
            except IndexError:
                pass

    # logger.debug("Is valid plan? {}".format(is_valid_plan))
    return is_valid_plan

group = group_right

prev_pos = tuple()

trajectories = dict()

plan_name = "none"




def save_everything(plan_name):
    csv_columns = ['x','y','z']
    t = time.time()
    for traj in trajectories:
        with open(str(Path.joinpath(project_path,'plan_data/realrobot/{}.csv'.format(plan_name))), 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in trajectories[plan_name]:
                writer.writerow(data)

def rob_pos_callback(msg):
    global prev_pos
    trajectories[plan_name] = trajectories.get(plan_name, [])

    if prev_pos != msg.ee:
        print(msg.ee)
        trajectories[plan_name].append({
            'x': msg.ee[0],
            'y': msg.ee[1],
            'z': msg.ee[2]
        })
    prev_pos = msg.ee

pos_sub = rospy.Subscriber('positions_robot_right_arm', RobotArm, rob_pos_callback)

n_trials = 10
trial = 0

plans = []

if args.pipeline == "stomp":
    planners = ["STOMP"]
else:
    planners = ["BiTRRT", "RRTConnect"]
    
robot_client.setNamedTarget("ready", "right", move=True)

goals = ["home","dropoff","test_grasp","random3"]

for planner in planners:
    logger.info(planner)
    group_right.set_planner_id(planner)
    group_right.set_planning_time(1)
    for goal in goals:
        while trial < n_trials:

            plan_name = "ready-to-{}-{}-{}".format(goal,planner,trial)
            plans.append(plan_name)
            logger.debug(plan_name)
            robot_client.setNamedTarget(goal, "right", move=True)

            plan_name = "{}-to-ready-{}-{}".format(goal,planner,trial)
            plans.append(plan_name)
            logger.debug(plan_name)

            robot_client.setNamedTarget("ready", "right", move=True)
            trial += 1
        trial = 0

print("Done with plans, saving to CSV")
print(trajectories)
for plan in plans:
    save_everything(plan)

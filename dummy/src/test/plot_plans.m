trials = [0:9];
% planners = ["SBL","EST","LBKPIECE","BKPIECE","KPIECE","RRT","RRTConnect","RRTstar","TRRT","PRM","PRMstar","FMT","BFMT","PDST","STRIDE","BiTRRT","LBTRRT","BiEST","ProjEST","LazyPRM","LazyPRMstar","SPARS","SPARStwo"];
% planners = ["STOMP"]
planners = ["STOMP","BiTRRT", "RRTConnect"]

paths = ["ready-to-home","ready-to-test_grasp","ready-to-random3","home-to-ready", "test_grasp-to-ready", "random3-to-ready"];

test = "realrobot" %you can change this to realrobot

% create output directories if they don't exist
if ~exist("plan_plots", 'dir')
       mkdir("plan_plots")
end
if ~exist(strcat("plan_plots/",test), 'dir')
       mkdir(strcat("plan_plots/",test))
end

for t = 1:length(paths)
    for p = 1:length(planners)
        try
            for i = 1:length(trials)
                data = readtable(strcat("plan_data/",test,"/",paths(t),"-",planners(p),"-",int2str(trials(i)),".csv"));

                x = table2array(data(:,1));
                y = table2array(data(:,2));
                z = table2array(data(:,3));

                plot3(x,y,z, '-o', 'MarkerIndices',[1,length(x)])
                hold on 

            end
            axis equal
            title(planners(p))
            xlabel('x (m)')
            ylabel('y (m)')
            zlabel('z (m)')
            if contains(paths(t),"grasp")
                ylim([0 0.8]);
                xlim([-0.2 0.7]);
                zlim([1.2 2]);
                view(3.801370469182386e+02,18.467347528785851);
            else
                ylim([-0.5 1]);
                xlim([0 1]);
                zlim([1 2]);
                view(2.503474497489790e+02,8.948805866205749);
            end



            saveas(gcf,strcat("plan_plots/",test,"/",paths(t),"-",planners(p),".png"))
            hold off
        catch
                warning(strcat(planners(p)," ",paths(t)," - failed to open csv, skipping"))
        end
    end
end
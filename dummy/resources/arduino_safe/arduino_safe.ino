#include <ros.h>
#include <std_msgs/ColorRGBA.h>

ros::NodeHandle nh;
const int red = 3;
const int green = 5;
const int blue = 6;


void messageCb( const std_msgs::ColorRGBA& msg){
  analogWrite(red,msg.r);
  analogWrite(green,msg.g);
  analogWrite(blue,255.0-msg.b);
}

ros::Subscriber<std_msgs::ColorRGBA> sub("led_strip", messageCb );

void setup()
{
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);
  nh.initNode();
  nh.subscribe(sub);
}

void loop()
{
  nh.spinOnce();
  //delay(500);
}

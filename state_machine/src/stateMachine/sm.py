#!/usr/bin/env python

import rospy
import smach
import smach_ros

import threading
from vision_utils.logger import get_logger

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--sim', action='store_true')
args, unknown = parser.parse_known_args()

logger=get_logger()

rospy.init_node('robot_safety_state_machine')

from states import Ready, Wave, Handover, Robot_working, set_sim
from std_msgs.msg import Bool, String
from state_machine.msg import UIData
from safety.msg import RobotArm, Distance, SafetyInfo
set_sim(args.sim)

# Create a SMACH state machine
sm = smach.StateMachine(outcomes=["error"])
sm.userdata.action = "none"
sm.userdata.zone = "red"
sm.userdata.move_status = "none done"
sm.userdata.prev_state = "none"
current_state = "ready"

def action_callback(msg):
    sm.userdata.action = msg.data
    # logger.debug("Got action {}".format(sm.userdata.action))

def zone_callback(msg):
    sm.userdata.zone = msg.zone
    # logger.debug("Got zone {}".format(sm.userdata.zone))

def move_callback(msg):
    sm.userdata.move_status = msg.data
    # logger.debug("Got move_status {}".format(sm.userdata.move_status))

def state_callback(msg):
    global current_state
    current_state = msg.data

rospy.Subscriber("/action_recognition", String, action_callback)
rospy.Subscriber("/safety", SafetyInfo, zone_callback)
rospy.Subscriber("/move_status", String, move_callback)
rospy.Subscriber("/sm_state", String, state_callback)
ui_pub = rospy.Publisher('/ui_commander',UIData,queue_size=1)
sim_pub = rospy.Publisher('/use_sim',Bool,queue_size=1)

# Open the container
with sm:
    # Add states to the container
    smach.StateMachine.add('READY', Ready(),
                            transitions={'waving': 'WAVE',
                                        "handover": "HANDOVER",
                                         "working": "WORKING",
                                         "distracted": "READY",
                                         "none": "READY"},
                            remapping={'action':'action', 'zone':'zone', 'prev_state':'prev_state', 'move_status':'move_status'})
    smach.StateMachine.add('WAVE', Wave(),
                           transitions={'waving': 'WAVE',
                                        "handover": "HANDOVER",
                                        "working": "WORKING",
                                        "distracted": "READY",
                                        "none": "READY"},
                           remapping={'action':'action', 'zone':'zone', 'prev_state':'prev_state', 'move_status':'move_status'})

    smach.StateMachine.add('WORKING', Robot_working(),
                           transitions={'waving': 'WAVE',
                                        "handover": "HANDOVER",
                                        "working": "WORKING",
                                        "distracted": "WORKING",
                                        "none": "WORKING"},
                           remapping={'action':'action', 'zone':'zone', 'prev_state':'prev_state', 'move_status':'move_status'})

    smach.StateMachine.add('HANDOVER', Handover(),
                           transitions={'waving': 'WAVE',
                                        "handover": "HANDOVER",
                                        "working": "WORKING",
                                        "distracted": "WORKING",
                                        "none": "READY"},
                           remapping={'action':'action', 'zone':'zone', 'prev_state':'prev_state', 'move_status':'move_status'})


sis  = smach_ros.IntrospectionServer('lh7', sm, '/SM_ROOT')
sis.start()
# Execute SMACH plan
# outcome = sm.execute()
smach_thread = threading.Thread(target=sm.execute)
smach_thread.start()

def update_ui(action, state, move_status, zone):
    msg = UIData()
    msg.safety_status = zone
    msg.user_action = action
    msg.robot_state = state
    msg.move_status = move_status
    ui_pub.publish(msg)

while not rospy.is_shutdown():
    sim_pub.publish(args.sim)
    update_ui(state=current_state, action=sm.userdata.action, zone=sm.userdata.zone, move_status=sm.userdata.move_status)
    rospy.sleep(0.1)

sm.request_preempt()

smach_thread.join()

sis.stop()

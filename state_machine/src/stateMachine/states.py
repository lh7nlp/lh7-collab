#!/usr/bin/env python3

import rospy
import smach
from stateMachine.utils import TopicToOutcomeState, WaitForMsgState
from std_msgs.msg import String, Bool, Float32MultiArray

SIM = False
def set_sim(sim):
    global SIM
    SIM = sim

move = True
wait_for_movement = True

action_to_boundary = {
    'handover': [0.1, 1],
    'waving': [0.3, 1],
    'working': [0.5, 1.5],
    'distracted': [1, 2],
    'none': [1, 2]
}

def check_action_for_leds(action, zone):
    if action == "distracted":
        led_pub.publish("blink {}".format(zone))
    else:
        led_pub.publish("const {}".format(zone))

def check_action_for_safety(action):
    msg = Float32MultiArray()
    msg.data = action_to_boundary[action]
    safety_pub.publish(msg)

led_pub = rospy.Publisher('/led_commander',String,queue_size=1)
move_pub = rospy.Publisher('/move_commander',String,queue_size=1)
safety_pub = rospy.Publisher('/safety_commander',Float32MultiArray,queue_size=1)
sm_pub = rospy.Publisher('/sm_state',String,queue_size=1)

def get_movement_action(move_status):
    if move_status.split(" ")[-1] == "done":
        return "done"
    else:
        return move_status.split(" ")[0]

def check_data(userdata):
    check_action_for_safety(userdata.action)
    check_action_for_leds(userdata.action, userdata.zone)

    rospy.sleep(0.5)

class Ready(smach.State):
    def __init__(self):
        smach.State.__init__(self,
            outcomes=["waving", "handover", "distracted", "working", "none"],
            input_keys=['action','zone', 'prev_state', "move_status"],
            output_keys=['prev_state'])

    def execute(self, userdata):
        rospy.loginfo('Executing state Ready')
        if move: move_pub.publish("close_gripper")
        sm_pub.publish("ready")
        userdata.prev_state = "ready"
        if userdata.action in self._outcomes:
            check_data(userdata)
            movement = get_movement_action(userdata.move_status)
            print(movement)
            if movement == "done" or not wait_for_movement:
                return userdata.action
            else:
                return movement
        else:
            return "none"


class Wave(smach.State):
    def __init__(self):
        smach.State.__init__(self,
            outcomes=["waving", "handover", "distracted", "working", "none"],
            input_keys=['action','zone', 'prev_state', "move_status"],
            output_keys=['prev_state'])

    def execute(self, userdata):
        rospy.loginfo('Executing state Wave')
        if move: move_pub.publish("wave")
        sm_pub.publish("wave")
        userdata.prev_state = "wave"
        if userdata.action in self._outcomes:
            check_data(userdata)
            movement = get_movement_action(userdata.move_status)
            if movement == "done" or not wait_for_movement:
                return userdata.action
            else:
                return movement
        else:
            return "none"

class Handover(smach.State):
    def __init__(self):
        smach.State.__init__(self,
            outcomes=["waving", "handover", "distracted", "working", "none"],
            input_keys=['action','zone', 'prev_state', "move_status"],
            output_keys=['prev_state'])

    def execute(self, userdata):
        rospy.loginfo('Executing state Handover')
        if move: move_pub.publish("handover")
        sm_pub.publish("handover")
        userdata.prev_state = "handover"
        if userdata.action in self._outcomes:
            check_data(userdata)
            movement = get_movement_action(userdata.move_status)
            if movement == "done" or not wait_for_movement:
                return userdata.action
            else:
                return movement
        else:
            return "none"

class Robot_working(smach.State):
    def __init__(self):
        smach.State.__init__(self,
            outcomes=["waving", "handover", "distracted", "working", "none"],
            input_keys=['action','zone', 'prev_state', "move_status"],
            output_keys=['prev_state'])

    def execute(self, userdata):
        rospy.loginfo("Executing state Robot_working")
        if move: move_pub.publish("work")
        sm_pub.publish("working")

        if userdata.prev_state != "working":
            if move: move_pub.publish("open_gripper")

        userdata.prev_state = "working"
        if userdata.action in self._outcomes:
            check_data(userdata)
            movement = get_movement_action(userdata.move_status)
            if movement == "done" or not wait_for_movement:
                return userdata.action
            else:
                return movement
        else:
            return "none"

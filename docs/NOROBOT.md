This file explains how system functionality can be partially run/tested without any connection to the LH7+ platform.

### Simulation without any connection to LH7+

To launch the robot model and fake controller:
```shell
roslaunch dummy main.launch mode:=norobot USB:=false
roslaunch dummy main.launch mode:=norobot USB:=false pipeline:=stomp # a specific MoveIt! motion planning pipeline can be specified: stomp, chomp or ompl (default)
```

If no RealSense camera is connected, the human pose estimation module can use a webcam input (limited functionality since there's is no depth data):
```shell
roslaunch human_pose_ROS openpifpaf_webcam.launch
```
The markers are published on ``/markers_openpifpaf_pose_transformed_pose_world``, and the RGB image processed by OpenPifPaf are published on ``/openpifpaf_img`` (both can be visualized in Rviz - make sure the fixed frame is set to ``world``).

To start action recognition (note that this will not give desired results as the pose lacks any depth data, and is not being transformed to 3D metric coordinates):
```shell
rosrun pose_classificion classify_pose.py --cam no
```
The predicted action is published on ``rostopic echo /action_recognition``.

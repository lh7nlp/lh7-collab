# Sub-system tests

## Human pose estimation

### Comparison of human pose methods

First, download the dataset from [here](https://drive.google.com/file/d/1TS3ZvR3fIW_vBDaG9iYajHUAw1YQE38W/view?usp=sharing) and copy the dataset images into the ``human_pose_ROS/src/eval/supervisely`` folder. The evaluation script can then be run for each pose estimation method as follows:
```shell
cd human_pose_ROS/src/eval
python3 predictions_vs_supervisely.py --method {method} --scale {scale}
```
* supported methods: ``openpifpaf`` (default), ``pytorch-pose-hg-3d``, ``pytorch_Realtime_Multi-Person_Pose_Estimation``, ``fastpose-drnoodle``, ``fastpose-zexinchen``, ``openpose``, ``realsense_sdk``, ``random``
* image scaling factor: ``0.5`` or ``1`` (default)

The evaluation metrics will be shown in the terminal, and the resulting images are stored in the method's folder eg. ``human_pose_ROS/src/eval/openpifpaf``.

### Measurement noise

```shell
rosrun human_pose_ROS save_joints.py
```

Results can then be plotted in Matlab with ``human_pose_ROS/src/pose_utils/distribution_plots.m``.

## Action recognition model

### Collecting training data

Record data as follows:
```shell
rosrun pose_classificion save_to_txt.py --person jan --action working
```
When the script is interrupted with Ctrl+C, all the recorded data is saved as .txt and .gif files in ``pose_classification/src/action_data``. For additional visualization, gifs of the skeleton sequences can be generated with
```shell
rosrun pose_classificion plot-deepgru-txt.py
```

### Training the model

```shell
cd pose_classification/src/DeepGRU
python3 main.py --dataset lh7 # train on our custom dataset
python3 main.py --dataset sbu # train on SBU-Kinect dataset
```
The best model (checkpoint based on maximum validation accuracy) for each fold is saved in ``pose_classification/src/DeepGRU/save``, along with the learning curves. To generate additional evaluation plots (eg. confusion matrix), run ``pose_classification/src/DeepGRU/eval.py`` (make sure the paths in the `models` dictionary are updated).

## Motion planning tests

### MoveIt! benchmarking

First, follow the instructions [here](https://github.com/ros-planning/warehouse_ros_mongo#building-from-source) and make sure ``warehouse_ros_mongo`` is built with ``catkin build warehouse_ros_mongo``.

note to self: need to back-up the DB from the desktop so this can work

Launch the MongoDB where the motion planning queries are stored:
```shell
roslaunch dummy db.launch
```

Start the benchmark:
```shell
roslaunch dummy benchmark.launch
```
This will load the benchmark configuration specified in ``dummy/config/benchmark/benchmarks_final.yaml`` and save the results in ``dummy/moveit_benchmark_logs``.

### Plotting trajectories

Recorded trajectories (stored in ``dummy/src/test/plan_data`` as CSV files) can be visualized in Matlab by running  ``dummy/src/test/plot_plans.m``. The script stores the plots in ``dummy/src/test/plan_plots``.

To re-run the tests, first launch the robot:
```shell
roslaunch dummy main.launch mode:=norobot pipeline:=stomp # move in simulation with fake controller
roslaunch dummy main.launch mode:=realrobot pipeline:=stomp # move real robot
```
Then execute the test queries:
```shell
rosrun dummy plot_plans.py --pipeline stomp
```
Note: the motion planning pipeline can be ``stomp`` or ``ompl``. The robot connection should be re-launched when the pipeline is changed.

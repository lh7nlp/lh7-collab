#! /usr/bin/env python3

import rospy
import numpy as np
from human_pose_ROS.msg import Skeleton, PoseEstimation
from rospy_message_converter import message_converter
from std_msgs.msg import Float32, String
from sensor_msgs.msg import Image, CameraInfo
from vision_utils.logger import get_logger, get_printer
from vision_utils.timing import CodeTimer
from vision_utils.img import image_to_numpy
from pose_utils.utils import *
import vg
import argparse
from pathlib import Path
import imageio
import cv2
import time

project_path = Path(__file__).parent.absolute()

logger = get_logger()
pp = get_printer()

parser = argparse.ArgumentParser()
parser.add_argument('--debug',
                    default=True,
                 action='store_true')
parser.add_argument('--cam', default='wrist')
args, unknown = parser.parse_known_args()

rospy.init_node("mainaction_eval")

DEPTH_INFO_TOPIC = '/{}_camera/camera/aligned_depth_to_color/camera_info'.format(args.cam)

num_images = 0

saving = False

images = []

latest_action = "none"

import os
if not os.path.exists(Path.joinpath(project_path,f"mainaction_data")):
    os.makedirs(Path.joinpath(project_path,f"mainaction_data"))

save_path = Path.joinpath(project_path,f"mainaction_data/{time.time()}.mp4")

if args.cam in ["wrist","base"]:
    logger.info("Waiting for camera info :)")
    cameraInfo = rospy.wait_for_message(DEPTH_INFO_TOPIC, CameraInfo)
    logger.info("Got camera info")


def save_everything():
    save_images(images)

def save_images(images, name=time.time()):
    height,width,layers=images[0].shape
    fourcc = cv2.VideoWriter_fourcc(*'H264')

    video=cv2.VideoWriter(str(save_path),fourcc,10,(width,height))

    for image in images:
        video.write(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

    video.release()

# def save_images(images):
#     with imageio.get_writer(save_path, mode='I') as writer:
#         for image in images:
#             writer.append_data(image)


def img_cb(msg):
    global images, num_images

    if not saving:
        print("got image")
        # path = msg.data
        num_images += 1
        font = cv2.FONT_HERSHEY_SIMPLEX
        image = cv2.putText(image_to_numpy(msg), latest_action, (10,220), font, 2, (0, 255, 0), 2, cv2.LINE_AA)
        images.append(image)



def action_cb(msg):
    global latest_action
    if not saving:
        print("got action")
        latest_action = msg.data

pose_sub = rospy.Subscriber('action_recognition', String, action_cb)
poseimg_sub = rospy.Subscriber('openpifpaf_img', Image, img_cb)

import signal, sys


def signal_handler(sig, frame):
    global saving
    if not saving:
        saving = True
        logger.warning("Saving everything")
        save_everything()
        sys.exit(1)

signal.signal(signal.SIGINT, signal_handler)

rospy.spin()

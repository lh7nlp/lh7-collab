#! /usr/bin/env python3

import rospy
import numpy as np
from human_pose_ROS.msg import Skeleton, PoseEstimation
from rospy_message_converter import message_converter
from std_msgs.msg import Float32, String
from sensor_msgs.msg import Image, CameraInfo
from vision_utils.logger import get_logger, get_printer
from vision_utils.timing import CodeTimer
from vision_utils.img import image_to_numpy
from pose_utils.utils import *
import vg
import argparse
from pathlib import Path
import imageio
from scipy.stats import mode
import time

from DeepGRU.predict import run_inference

project_path = Path(__file__).parent.absolute()

logger = get_logger()
pp = get_printer()

parser = argparse.ArgumentParser()
parser.add_argument('--debug',
                    default=True,
                 action='store_true')
parser.add_argument('--seqlength',
                    default=20)
parser.add_argument('--cam', default='wrist')
args, unknown = parser.parse_known_args()

rospy.init_node("save_pose")

save_points = ["eye", "nose", "shoulder", "elbow", "wrist"]

DEPTH_INFO_TOPIC = '/{}_camera/camera/aligned_depth_to_color/camera_info'.format(args.cam)

im_h = 480
im_w = 848

num_frames = 0
num_images = 0

saving = False

frames = {}

joint_coords = []
images = []

FRAMES_PER_SEQ = args.seqlength
predictions = []

import os
if not os.path.exists(Path.joinpath(project_path,f"action_data")):
    os.makedirs(Path.joinpath(project_path,f"action_data"))

if args.cam in ["wrist","base"]:
    logger.info("Waiting for camera info :)")
    cameraInfo = rospy.wait_for_message(DEPTH_INFO_TOPIC, CameraInfo)
    logger.info("Got camera info")

last_action = time.time()

def points_cb(msg):
    global num_frames, num_images, images, frames, last_action

    if not saving:
        num_frames += 1
        if len(msg.skeletons):
            skeleton = msg.skeletons[0]
            msg_dict = message_converter.convert_ros_message_to_dictionary(skeleton)
            msg_dict = {k: v for k, v in msg_dict.items() if isinstance(v, list) and k.split("_")[-1] in save_points}
            msg_dict_tf = dict()
            pnts = []
            for i,v in msg_dict.items():
                pnt1_cam = v if len(v) else [0,0,0]
                msg_dict_tf[i] = pnt1_cam
                pnts.extend(pnt1_cam)

            joint_coords.append([str(i) for i in pnts])
            last_action = time.time()

if args.cam in ["wrist","base"]:
    pose_sub = rospy.Subscriber('openpifpaf_pose_transformed_pose_cam', PoseEstimation, points_cb)
else:
    pose_sub = rospy.Subscriber('openpifpaf_pose_transformed_pose_world', PoseEstimation, points_cb)

action_pub = rospy.Publisher('action_recognition', String, queue_size=1)

sequence_number = 0



while not rospy.is_shutdown():
    if time.time() - last_action < 3:
        if len(joint_coords)>FRAMES_PER_SEQ:
            with CodeTimer() as timer:
                sequence_number += 1
                joint_coords_copy = joint_coords[-FRAMES_PER_SEQ:]
                prediction = run_inference(joint_coords_copy)[0]
            logger.debug(f"Sequence {sequence_number} took {timer.took} ms")
            if prediction['conf'] > 0.8:
                predictions.append(prediction['label'])
            elif prediction['conf'] < 0.5:
                predictions.append("none")
            joint_coords = joint_coords[-FRAMES_PER_SEQ:]
                    # action_pub.publish(prediction['label'])
                # joint_coords = joint_coords[-5:]

        if len(predictions)>6:
            main_action = mode(predictions)[0][0]
            predictions = predictions[-6:]
            # logger.warning(f"Main action: {main_action}")
            action_pub.publish(main_action)
    else:
        action_pub.publish("none")

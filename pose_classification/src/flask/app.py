#! /usr/bin/env python3

import rospy

from rospy_message_converter import message_converter
from std_msgs.msg import Float32, String
from sensor_msgs.msg import Image, CameraInfo
from state_machine.msg import UIData


from flask import Flask, render_template, session, copy_current_request_context
from flask_socketio import SocketIO, emit, disconnect

async_mode = None
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socket_ = SocketIO(app)

action_color_mappings = {
    "waving": "pink",
    "distracted": "orange",
    "working": "blue",
    "handover": "green"
}

action_image_mappings = {
    "waving": "static/img/wave.gif",
    "distracted": "static/img/distracted.gif",
    "working": "static/img/busy.gif",
    "handover": "static/img/handover.png"
}

action_text_mappings = {
    "waving": "Hey human! :D",
    "distracted": "If you could pay attention to me, that would be great...",
    "working": "You seem very busy with something",
    "handover": "I see that you're trying to hand me something"
}

state_text_mappings = {
    "wave": "You can try giving me an object",
    "ready": "I'm ready",
    "working": "I'm working on something important",
    "handover": "I'm ready to receive an object"
}

state_color_mappings = {
    "waving": "white",
    "ready": "white",
    "working": "white",
    "handover": "white"
}

move_text_mappings = {
    "open_gripper": "I'm opening my hand",
    "close_gripper": "Closing my fist, be careful",
    "up_with_object": "I'm moving up with the object",
    "up_without_object": "I'm moving up",
    "down_to_object": "I'm moving down to the object",
    "move_to_object": "I'm moving to the object",
    "left_with_object": "I'm moving left with the object",
    "right_with_object": "I'm moving right with the object",
    "wave_right": "I'm moving my wrist",
    "wave_left": "I'm moving my wrist",
    "to_handover_pose": "I'm stretching out my arm",
    "done": ""
}

move_image_mappings = {
    "open_gripper": "static/img/ungrasp.gif",
    "close_gripper": "static/img/grasp.gif",
    "up_with_object": "static/img/up.gif",
    "up_without_object": "static/img/up.gif",
    "left_with_object": "static/img/left.gif",
    "right_with_object": "static/img/right.gif",
    "down_to_object": "static/img/down.gif",
    "move_to_object": "static/img/moving.gif",
    "wave_left": "static/img/ur5wave.gif",
    "wave_right": "static/img/ur5wave.gif",
    "to_handover_pose": "",
    "done": ""
}

@app.route('/')
def index():
    return render_template('index.html', async_mode=socket_.async_mode)

def got_data(msg):
    # print(msg)

    msg.move_status = msg.move_status.split(" ")[-1]

    data = {
    'action': {
        'img': action_image_mappings.get(msg.user_action, "static/img/confused.jpg"),
        'name': msg.user_action,
        'text': action_text_mappings.get(msg.user_action, "I'm not sure what you're doing right now..."),
        'color': action_color_mappings.get(msg.user_action, "black")
        },
    'state': {
        'text': state_text_mappings.get(msg.robot_state, "I'm not sure what I'm doing to be honest :s"),
        'color': state_color_mappings.get(msg.robot_state, "white")
    },
    'move': {
        'text': move_text_mappings.get(msg.move_status, "I might be moving, I might not..."),
        'color': "grey",
        'img': move_image_mappings.get(msg.move_status, "static/img/confused.jpg")
    }
    }
    socket_.emit('status',data)

rospy.init_node("flask_app")
rospy.Subscriber('/ui_commander', UIData, got_data)

if __name__ == '__main__':
    socket_.run(app, debug=True)

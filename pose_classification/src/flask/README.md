```
flask run --no-reload --host=0.0.0.0 # to run
ngrok http localhost:5000 # create local tunnel
lt --port 5000 --subdomain lh7-collab # better local tunnel
sudo kill -9 $(sudo lsof -t -i:5000) # to kill
```

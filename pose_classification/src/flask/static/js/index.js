// import * as tts from './tts.js';

// var $ = require("jquery");

$( document ).ready(function() {
    var socket = io('/');
    console.log('socket')

    socket.on('connect', function() {
        socket.emit('my_event', {data: 'connected to the SocketServer...'});
    });

    socket.on('status', function(msg, cb) {
        console.log(msg);

        if(msg.action.img != $("#action_img").attr("src")) {
            $("#action_img").attr("src",msg.action.img);
            // tts.sayText(msg.action.text);
        }

        $("#action_txt").text(msg.action.text);
        $("#action_txt_container").css('background-color', msg.action.color);

        $("#state_txt").text(msg.state.text);
        $("#state_txt_container").css('background-color', msg.state.color);

        if(msg.move.img != $("#move_img").attr("src")) {
            $("#move_img").attr("src",msg.move.img);
            // tts.sayText(msg.move.text);
        }
        $("#move_txt").text(msg.move.text);
        $("#move_txt_container").css('background-color', msg.move.color);
    });
});
